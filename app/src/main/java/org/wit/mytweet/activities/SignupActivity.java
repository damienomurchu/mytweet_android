package org.wit.mytweet.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.wit.android.helpers.IntentHelper;
import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.User;
import static org.wit.android.helpers.IntentHelper.navigateUp;

/**
 * Signup class to govern actions and functionality around the signup view
 */
public class SignupActivity extends AppCompatActivity
  implements View.OnClickListener
{
  private TextView firstName;
  private TextView lastName;
  private TextView password;
  private TextView email;
  private Button signup;

  private MyTweetApp app;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_signup);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    app = (MyTweetApp) getApplication();

    //assign widgets
    firstName = (TextView) findViewById(R.id.signupFirstName);
    lastName  = (TextView) findViewById(R.id.signupLastName);
    email     = (TextView) findViewById(R.id.signupEmail);
    password  = (TextView) findViewById(R.id.signupPassword);
    signup    = (Button)   findViewById(R.id.signupButton);

    // set listener
    signup.setOnClickListener(this);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      // up button presses
      case android.R.id.home:
        navigateUp(this);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onClick(View view)
  {
    switch (view.getId())
    {
      // signup button presses
      case R.id.signupButton:
        User user = new User(firstName.getText().toString(), lastName.getText().toString(),
                                email.getText().toString(), password.getText().toString());
        app.newUser(user);
        IntentHelper.startActivity(this, LoginActivity.class);
    }
  }
}