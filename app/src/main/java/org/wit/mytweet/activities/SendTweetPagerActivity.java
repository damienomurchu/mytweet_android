package org.wit.mytweet.activities;

import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentStatePagerAdapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.TweetList;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Class to govern all actions surrounding the sending of a tweet
 */
public class SendTweetPagerActivity extends AppCompatActivity
{
  private ViewPager viewPager;
  private PagerAdapter pagerAdapter;

  private ArrayList<Tweet> tweets;
  private TweetList tweetList;


  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    viewPager = new ViewPager(this);
    viewPager.setId(R.id.viewPager);
    setContentView(viewPager);
    setTweetList();
    pagerAdapter = new PagerAdapter(getFragmentManager(), tweets);
    viewPager.setAdapter(pagerAdapter);
    setCurrentItem();
  }

  /**
   * Private helper to retrieve saved tweetlist
   */
  private void setTweetList()
  {
    MyTweetApp app = (MyTweetApp) getApplication();
    tweetList = app.tweetList;
    tweets = tweetList.getTweets();
  }

  /*
  * Ensure selected tweet is shown in details view
  */
  private void setCurrentItem()
  {
    UUID twtId = (UUID) getIntent().getSerializableExtra(SendTweetFragment.EXTRA_TWEET_ID);

    for (int i = 0; i < tweets.size(); i++)
    {
      if (tweets.get(i).getId().equals(twtId))
      {
        viewPager.setCurrentItem(i);
        break;
      }
    }
  }


  class PagerAdapter extends FragmentStatePagerAdapter
  {
    private ArrayList<Tweet> tweets;

    public PagerAdapter(FragmentManager fm, ArrayList<Tweet> tweets)
    {
      super(fm);
      this.tweets = tweets;
    }

    @Override
    public int getCount()
    {
      return tweets.size();
    }

    @Override
    public Fragment getItem(int pos)
    {
      Tweet tweet = tweets.get(pos);
      Bundle args = new Bundle();
      args.putSerializable(SendTweetFragment.EXTRA_TWEET_ID, tweet.getId());
      SendTweetFragment fragment = new SendTweetFragment();
      fragment.setArguments(args);
      return fragment;
    }

  }
}

