package org.wit.mytweet.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.wit.android.helpers.IntentHelper;
import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.User;

import static org.wit.android.helpers.IntentHelper.navigateUp;

/**
 * LoginActivity class that manages the interactions of the login view
 */
public class LoginActivity extends AppCompatActivity
    implements View.OnClickListener
{
  private EditText email;
  private EditText password;
  private Button login;

  private MyTweetApp app;

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    app = (MyTweetApp) getApplication();

    email = (EditText) findViewById(R.id.loginEmail);
    password = (EditText) findViewById(R.id.loginPassword);
    login = (Button) findViewById(R.id.loginButton);

    login.setOnClickListener(this);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      // up button presses
      case android.R.id.home:
        navigateUp(this);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  // OnClickListener overrides
  @Override
  public void onClick(View view)
  {
    switch(view.getId())
    {
      // login button presses
      case R.id.loginButton:
        String addr = email.getText().toString();
        String pass = password.getText().toString();
        if (app.validUser(addr, pass))
        {
          IntentHelper.startActivity(this, TweetListActivity.class);
        }
        else
        {
          IntentHelper.startActivity(this, WelcomeActivity.class);
        }
    }
  }

}
