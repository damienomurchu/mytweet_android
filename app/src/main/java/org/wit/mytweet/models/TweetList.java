package org.wit.mytweet.models;


import org.wit.mytweet.app.MyTweetApp;

import java.util.ArrayList;
import java.util.UUID;

import static org.wit.android.helpers.LogHelpers.info;

/**
 * TweetList class that represents a listeview of all tweets
 */
public class TweetList
{
  /**
   * Array that holds list of all tweets
   */
  private ArrayList<Tweet> tweets;

  /**
   * Serializer to serialize tweets to/ from file
   */
  private TweetListSerializer serializer;

  /**
   * Constructs a TweetList object from serialized file
   */
  public TweetList(TweetListSerializer serializer)
  {
    this.serializer = serializer;
    try
    {
      tweets = serializer.loadTweets();
    }
    catch (Exception e)
    {
      info(this, "Error loading tweets: " + e.getMessage());
      tweets = new ArrayList<Tweet>();
    }
  }

  /**
   * Saves tweets to file
   */
  public boolean saveTweets()
  {
    try
    {
      serializer.saveTweets(tweets);
      info(this, "Tweets saved to file");
      return true;
    }
    catch (Exception e)
    {
      info(this, "Error saving residences: " + e.getMessage());
      return false;
    }
  }

  /**
   * Saves tweets to file
   */
  public boolean clearTweets()
  {
    try
    {
      //this.serializer = new TweetListSerializer(MyTweetApp.class, MyTweetApp.getFilename);
      tweets = new ArrayList<Tweet>();
      serializer.saveTweets(tweets);
      info(this, "All tweets cleared");
      return true;
    }
    catch (Exception e)
    {
      info(this, "Error clearing tweets: " + e.getMessage());
      return false;
    }
  }

  /**
   * Returns the arraylist of tweets
   *
   * @return the arraylist of tweets
   */
  public ArrayList<Tweet> getTweets()
  {
    return tweets;
  }

  /**
   * Adds a tweet to the arraylist of tweets
   */
  public void addTweet(Tweet tweet)
  {
    tweets.add(tweet);
    saveTweets();
  }

  /**
   * Udates a tweet in the tweetlist
   */
  public void updateTweet(Tweet tweet)
  {
    // find tweet in tweetlist
    for (Tweet twt : tweets)
    {
      if (twt.getId().equals(tweet.getId()))
      {
        twt.setContent(tweet.getContent());
        saveTweets();
        info(this, "Updated Tweet content!");
      }
    }
  }

  /**
   * Deletes a tweet from the arraylist of tweets
   */
  public void deleteTweet(Tweet tweet)
  {
    tweets.remove(tweet);
    saveTweets();
  }

  /**
   * Generates sample tweets and populates TweetList with these tweets
   */
  private void generateSampleTweets()
  {
    for (int i = 0; i < 50; i++)
    {
      addTweet(new Tweet("Test tweet " + i));
    }
  }

  /**
   * Returns the tweet that corresponds to the id submitted
   *
   * @param id id of tweet to search for
   * @return the tweet that matches the id submitted
   */
  public Tweet getTweet(Long id)
  {
    for (Tweet tweet : tweets)
    {
      if (id.equals(tweet.getId()))
      {
        return tweet;
      }
    }
    return null;
  }

  /**
   * Returns the tweet that corresponds to the UUID submitted
   */
  public Tweet getTweet(UUID id)
  {
    for (Tweet tweet : tweets)
    {
      if (id.equals(tweet.getId()))
      {
        return tweet;
      }
    }
    return null;
  }

  public boolean containsTweet(Tweet tweet)
  {
    return tweets.contains(tweet);
  }

}
