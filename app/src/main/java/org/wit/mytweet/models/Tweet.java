package org.wit.mytweet.models;


import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;
import org.wit.mytweet.R;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * A model class that represents a Tweet and its associated functionality
 */
public class Tweet
{
  /**
   * Global limit on length of a tweet
   */
  public static final int length = 140;

  /**
   * Unique id number for the tweet
   */
  private UUID id;

  /**
   * Text content of a tweet
   */
  private String content;

  /**
   * Date when tweet was created
   */
  private Long date;

  /**
   * Email address for emailing tweet to a contact
   */
  public String email;

  //JSON key fields for serialisation
  private static final String JSON_ID = "id";
  private static final String JSON_CONTENT = "content";
  private static final String JSON_DATE = "date";
  private static final String JSON_EMAIL = "email";

  /**
   * Creates a new Tweet
   *
   * @param content String content of the Tweet
   */
  public Tweet(String content)
  {
    this.id = UUID.randomUUID();
    this.content = content;
    this.date = new Date().getTime();
    this.email = "No contact chosen";
  }

  /**
   * Creates a tweet from a json tweet object
   *
   * @param json
   * @throws JSONException
   */
  public Tweet(JSONObject json) throws JSONException
  {
    id = UUID.fromString(json.getString(JSON_ID));
    content = json.getString(JSON_CONTENT);
    date = json.getLong(JSON_DATE);
    email = json.getString(JSON_EMAIL);
  }

  /**
   * Transforms a tweet object into a json object
   *
   * @return
   * @throws JSONException
   */
  public JSONObject toJSON() throws JSONException
  {
    JSONObject json = new JSONObject();
    json.put(JSON_ID, id.toString());
    json.put(JSON_CONTENT, content);
    json.put(JSON_DATE, date.toString());
    json.put(JSON_EMAIL, email);

    return json;
  }

  /**
   * Returns the text content of a tweet
   *
   * @return the content of the tweet
   */
  public String getContent()
  {
    return content;
  }

  /**
   * Sets text content of the tweet
   *
   * @param content
   */
  public void setContent(String content)
  {
    this.content = content;
  }

  /**
   * Returns tweet date string
   *
   * @return
   */
  public String getDateString() {
    return dateString();
  }

  /**
   * Helper method that formats a date string
   *
   * @return formatted date string
   */
  private String dateString() {
    String dateFormat = "EEE d MMM yyyy H:mm";
    return android.text.format.DateFormat.format(dateFormat, date).toString();
  }

  /**
   * Returns a tweet that matches the UUID tweet id
   * @return
   */
  public UUID getId()
  {
    return id;
  }

  /**
   * Returns the string content of a tweet so it can be emailed
   *
   * @param context
   * @return
   */
  public String getEmailTweet(Context context) {
    String tweetedOn = context.getString(R.string.twtEmailTweetedOn);
    return tweetedOn + " " + getDateString() + ":\n\n" + getContent();

  }
}
