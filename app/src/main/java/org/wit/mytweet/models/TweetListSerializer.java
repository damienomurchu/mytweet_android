package org.wit.mytweet.models;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

/**
 * Created by dmurphy on 14/10/16.
 */

public class TweetListSerializer
{
  private Context mContext;
  private String mFilename;

  public TweetListSerializer(Context c, String f)
  {
    mContext = c;
    mFilename = f;
  }

  /**
   * Saves an arraylist of tweets to file
   *
   * @param tweets
   * @throws JSONException
   * @throws IOException
   */
  public void saveTweets(ArrayList<Tweet> tweets) throws JSONException, IOException
  {
    // build array in JSON
    JSONArray array = new JSONArray();
    for (Tweet tw : tweets)
    {
      array.put(tw.toJSON());
    }

    // write the file to disk
    Writer writer = null;
    try
    {
      OutputStream out = mContext.openFileOutput(mFilename, Context.MODE_PRIVATE);
      writer = new OutputStreamWriter(out);
      writer.write(array.toString());
    }
    finally
    {
      if (writer != null)
      {
        writer.close();
      }
    }
  }

  /**
   * Loads an arraylist of tweets from file
   *
   * @return
   * @throws IOException
   * @throws JSONException
   */
  public ArrayList<Tweet> loadTweets() throws IOException, JSONException
  {
    ArrayList<Tweet> tweets = new ArrayList<Tweet>();
    BufferedReader reader = null;

    try
    {
      // open and read file into a Stringbuilder
      InputStream in = mContext.openFileInput(mFilename);
      reader = new BufferedReader(new InputStreamReader(in));
      StringBuilder jsonString = new StringBuilder();
      String line = null;

      while((line = reader.readLine()) != null)
      {
        // line breaks are eliminated as irrelevant
        jsonString.append(line);
      }

      // parse the JSON using JSONTOkener
      JSONArray array = (JSONArray) new JSONTokener(jsonString.toString()).nextValue();
      // build array of tweets from JSONObjects
      for (int i = 0; i < array.length(); i++)
      {
        tweets.add(new Tweet(array.getJSONObject(i)));
      }
    }

    catch (FileNotFoundException e)
    {
      // ignored as happens when we start app fresh
    }

    finally
    {
      if (reader != null)
      {
        reader.close();
      }
    }

    return tweets;
  }
}
