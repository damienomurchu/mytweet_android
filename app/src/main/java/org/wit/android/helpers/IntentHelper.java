package org.wit.android.helpers;

import java.io.Serializable;

import android.app.Activity;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v4.app.NavUtils;

/**
 * IntentHelper class to assist with calling of activities
 */
public class IntentHelper
{
  /**
   * Helper method to start an activity
   *
   * @param parent
   * @param classname
   */
  public static void startActivity (Activity parent, Class classname)
  {
    Intent intent = new Intent(parent, classname);
    parent.startActivity(intent);
  }

  /**
   * Helper method to start and activity with data
   *
   * @param parent
   * @param classname
   * @param extraID
   * @param extraData
   */
  public static void startActivityWithData (Activity parent, Class classname, String extraID, Serializable extraData)
  {
    Intent intent = new Intent(parent, classname);
    intent.putExtra(extraID, extraData);
    parent.startActivity(intent);
  }

  /**
   * Helper method to start an activity with data and expect a result
   *
   * @param parent
   * @param classname
   * @param extraID
   * @param extraData
   * @param idForResult
   */
  public static void startActivityWithDataForResult (Activity parent, Class classname, String extraID, Serializable extraData, int idForResult)
  {
    Intent intent = new Intent(parent, classname);
    intent.putExtra(extraID, extraData);
    parent.startActivityForResult(intent, idForResult);
  }

  /**
   * Helper method to start an activity and expect a result
   *
   * @param parent
   * @param classname
   * @param idForResult
   */
  public static void startActivityForResult(Activity parent, Class classname, int idForResult)
  {
    Intent intent = new Intent(parent, classname);
    parent.startActivityForResult(intent, idForResult);
  }

  /**
   * Helper method to navigate up to parent activity
   *
   * @param parent
   */
  public static void navigateUp(Activity parent)
  {
    Intent upIntent = NavUtils.getParentActivityIntent(parent);
    NavUtils.navigateUpTo(parent, upIntent);
  }

  /**
   * Helper method to select contact from phone contacts
   *
   * @param parent
   * @param id
   */
  public static void selectContact(Activity parent, int id)
  {
    Intent selectContactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
    parent.startActivityForResult(selectContactIntent, id);
  }
}